package com.example.autocool;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;


import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Voitures extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voitures);


        final Button buttongoVoitures = (Button) findViewById(R.id.buttonQuitterVoitures);
        buttongoVoitures.setOnClickListener((View v) -> {
            finish();
        });


        try {
            listeCategvoitures();
        } catch (IOException e) {
            e.printStackTrace();
        }

        findViewById(R.id.buttonValiderLiteV).setOnClickListener(v -> {
            try {
                remplir();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }


    private void afficherList() {
        findViewById(R.id.listviewVoitures).setVisibility(View.VISIBLE);
    }


    public void listeCategvoitures() throws IOException {

        OkHttpClient client = new OkHttpClient();
        ArrayList arrayListVoitures = new ArrayList<String>();

        Request request = new Request.Builder()
                .url(getString(R.string.ipadresseCategV))
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {

            public void onResponse(Call call, Response response) throws IOException {

                String responseStr = response.body().string();
                JSONArray jsonArrayVoiture = null;



                try {
                    jsonArrayVoiture = new JSONArray(responseStr);

                    for (int i = 0; i < jsonArrayVoiture.length(); i++) {
                        JSONObject jsonClasse = null;
                        jsonClasse = jsonArrayVoiture.getJSONObject(i);
                        arrayListVoitures.add(jsonClasse.getString("LIBELLECATEG"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                
                runOnUiThread(() -> { //on met le code a tourner en asyncrone , on le fait mouliner de coté, on a donc l'inteface mais les operations elles se font de tek

                    Spinner spinnerVoitures = (Spinner) findViewById(R.id.SpinnerVoitures);

                    ArrayAdapter<String> arrayAdapterVoitures = new ArrayAdapter<String>(Voitures.this, android.R.layout.simple_spinner_item, arrayListVoitures);

                    spinnerVoitures.setAdapter(arrayAdapterVoitures);
                });
            }

            public void onFailure(Call call, IOException e) {
                Log.d("Test", "erreur!!! connexion impossible");
            }

        });
    }


    public void remplir() throws IOException {

        OkHttpClient client = new OkHttpClient();
        ArrayList arrayVoitures = new ArrayList<String>();
        final Spinner idCateg = findViewById(R.id.SpinnerVoitures);

        RequestBody formBody = new FormBody.Builder()
                .add("codeCateg", idCateg.getSelectedItem().toString())
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.ipadresseVoitures))
                .post(formBody)
                .build();


        Call call = client.newCall(request);

        call.enqueue(new Callback() {

            public void onResponse(Call call, Response response) throws IOException {
                String responseStr = response.body().string();
                JSONArray jsonArrayVoiture = null;

                Log.d("", responseStr+" Reponse du boutton valider quand on choisis la categorie");
                try {
                    jsonArrayVoiture = new JSONArray(responseStr);

                    for (int i = 0; i < jsonArrayVoiture.length(); i++) {
                        JSONObject jsonVoiture = null;
                        jsonVoiture = jsonArrayVoiture.getJSONObject(i);
                        arrayVoitures.add(" N° " + jsonVoiture.getString("NUMVEHICULE") + " " +
                                jsonVoiture.getString("LIEUSTATION") + " à " +
                                jsonVoiture.getString("VILLESTATION"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                runOnUiThread(() -> { //on met le code a tourner en asyncrone , on le fait mouliner de coté, on a donc l'inteface mais les operations elles se font de tek

                    ListView listviewV = (ListView) findViewById(R.id.listviewVoitures);

                    ArrayAdapter<String> arrayAdapterClasses = new ArrayAdapter<String>(Voitures.this, android.R.layout.simple_list_item_1, arrayVoitures);

                    listviewV.setAdapter(arrayAdapterClasses);

                    afficherList();

                    listviewV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            JSONArray ArrayVoiture = null;
                            try {
                                ArrayVoiture = new JSONArray(responseStr);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            JSONObject VoitureObject = null;
                            try {
                                VoitureObject = ArrayVoiture.getJSONObject(position);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("", VoitureObject.toString() +" objet tostring a envoyer a l'activité infoVoiture");

                            Intent intent = new Intent(Voitures.this, InfoVoiture.class);
                            intent.putExtra("laVoiture",VoitureObject.toString());
                            startActivity(intent);

                            listviewV.setVisibility(View.GONE);

                        }

                        ;
                    });
                });

            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }
        });
    }
}