package com.example.autocool;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {

    String responseStr ;
    OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        final Button buttongoVoitures = (Button)findViewById(R.id.GoVoitures);
        buttongoVoitures.setOnClickListener(v -> {

            Intent intent = new Intent(MainActivity.this, Voitures.class);
            startActivity(intent);

        });

        final Button buttonAjout = (Button)findViewById(R.id.AjoutVoitures);
        buttonAjout.setOnClickListener(v -> {

            Intent intent = new Intent(MainActivity.this, AjoutVoutureActivity.class);
            startActivity(intent);

        });
    }

}