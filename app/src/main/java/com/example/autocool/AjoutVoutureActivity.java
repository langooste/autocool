package com.example.autocool;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AjoutVoutureActivity extends AppCompatActivity {
    int codetypeV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_vouture);

        final Button btnAjoutV = (Button) findViewById(R.id.btnAjoutVoiture);

        final Spinner spinnerCategVoitures = (Spinner) findViewById(R.id.spinnerCategV);
        final Spinner spinnerTypeVoitures = (Spinner) findViewById(R.id.spinnerTypeV);
        final Spinner spinnerVilles = (Spinner) findViewById(R.id.spinnerVilles);
        final Spinner spinnerStation = (Spinner) findViewById(R.id.spinnerStations);

        NumberPicker nbpNVE = (NumberPicker) findViewById(R.id.nbpNvE);
        nbpNVE.setMinValue(0);
        nbpNVE.setMaxValue(100);

        try {
            listeCategvoitures(spinnerCategVoitures,spinnerTypeVoitures);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            listeVilles(spinnerVilles,spinnerStation);
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnAjoutV.setOnClickListener(v ->  {

            enregistrer();
            finish();
            });

    }



/***
 *      ____                               _   _
 *     |  _ \    ___   _ __ ___    _ __   | | (_)  ___   ___    __ _    __ _    ___
 *     | |_) |  / _ \ | '_ ` _ \  | '_ \  | | | | / __| / __|  / _` |  / _` |  / _ \
 *     |  _ <  |  __/ | | | | | | | |_) | | | | | \__ \ \__ \ | (_| | | (_| | |  __/
 *     |_| \_\  \___| |_| |_| |_| | .__/  |_| |_| |___/ |___/  \__,_|  \__, |  \___|
 *                                |_|                                  |___/
 *                    _                                  ____           _                                   _         __     __
 *      ___   _ __   (_)  _ __    _ __     ___   _ __   / ___|   __ _  | |_    ___    __ _    ___    _ __  (_)   ___  \ \   / /
 *     / __| | '_ \  | | | '_ \  | '_ \   / _ \ | '__| | |      / _` | | __|  / _ \  / _` |  / _ \  | '__| | |  / _ \  \ \ / /
 *     \__ \ | |_) | | | | | | | | | | | |  __/ | |    | |___  | (_| | | |_  |  __/ | (_| | | (_) | | |    | | |  __/   \ V /
 *     |___/ | .__/  |_| |_| |_| |_| |_|  \___| |_|     \____|  \__,_|  \__|  \___|  \__, |  \___/  |_|    |_|  \___|    \_/
 *           |_|                                                                     |___/
 */

    public void listeCategvoitures(Spinner spinCateg, Spinner spinType ) throws IOException {

        OkHttpClient client = new OkHttpClient();
        ArrayList arrayListVoitures = new ArrayList<String>();

        Request request = new Request.Builder()
                .url(getString(R.string.ipadresseCategV))
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {


            public void onResponse(Call call, Response response) throws IOException {

                String responseStr = response.body().string();
                JSONArray jsonArrayVoiture = null;



                try {
                    jsonArrayVoiture = new JSONArray(responseStr);

                    for (int i = 0; i < jsonArrayVoiture.length(); i++) {
                        JSONObject jsonClasse = null;
                        jsonClasse = jsonArrayVoiture.getJSONObject(i);
                        arrayListVoitures.add(jsonClasse.getString("LIBELLECATEG"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                runOnUiThread(() -> { //on met le code a tourner en asyncrone , on le fait mouliner de coté, on a donc l'inteface mais les operations elles se font de tek

                    ArrayList<String> oui = new ArrayList<>();
                    oui.add("Selectionnez une categorie");
                    ArrayAdapter<String> arrayAdapterVoitures = new ArrayAdapter<String>(AjoutVoutureActivity.this, android.R.layout.simple_spinner_item, oui);

                    spinCateg.setAdapter(arrayAdapterVoitures);

                    spinCateg.setOnTouchListener(new View.OnTouchListener(){

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            ArrayAdapter<String> arrayAdapterVoitures = new ArrayAdapter<String>(AjoutVoutureActivity.this, android.R.layout.simple_spinner_item, arrayListVoitures);
                            spinCateg.setAdapter(arrayAdapterVoitures);

                            spinCateg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {

                                    String item = (String) spinCateg.getSelectedItem();
                                    Log.d("test",item);
                                    try {
                                        remplirTypeV(item,spinType);
                                    } catch (IOException e) {
                                        Log.d("test", "le remplissage du spinCateg ne marche pas =(");
                                    }
                                    spinType.setVisibility(View.VISIBLE);
                                }
                                public void onNothingSelected(AdapterView<?> arg0) { }
                            });
                            return false;
                        }

                    });
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }
        });
    }

    /***
     *      ____                               _   _
     *     |  _ \    ___   _ __ ___    _ __   | | (_)  ___   ___    __ _    __ _    ___
     *     | |_) |  / _ \ | '_ ` _ \  | '_ \  | | | | / __| / __|  / _` |  / _` |  / _ \
     *     |  _ <  |  __/ | | | | | | | |_) | | | | | \__ \ \__ \ | (_| | | (_| | |  __/
     *     |_| \_\  \___| |_| |_| |_| | .__/  |_| |_| |___/ |___/  \__,_|  \__, |  \___|
     *                                |_|                                  |___/
     *                    _                                 _____                         __     __         _       _                  _
     *      ___   _ __   (_)  _ __    _ __     ___   _ __  |_   _|  _   _   _ __     ___  \ \   / /   ___  | |__   (_)   ___   _   _  | |   ___
     *     / __| | '_ \  | | | '_ \  | '_ \   / _ \ | '__|   | |   | | | | | '_ \   / _ \  \ \ / /   / _ \ | '_ \  | |  / __| | | | | | |  / _ \
     *     \__ \ | |_) | | | | | | | | | | | |  __/ | |      | |   | |_| | | |_) | |  __/   \ V /   |  __/ | | | | | | | (__  | |_| | | | |  __/
     *     |___/ | .__/  |_| |_| |_| |_| |_|  \___| |_|      |_|    \__, | | .__/   \___|    \_/     \___| |_| |_| |_|  \___|  \__,_| |_|  \___|
     *           |_|                                                |___/  |_|
     */
    public void remplirTypeV(String CategV, Spinner spinType) throws IOException {

        OkHttpClient client = new OkHttpClient();
        ArrayList arrayCategV = new ArrayList<String>();

        RequestBody formBody = new FormBody.Builder()
                .add("codeCateg", CategV )
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.ipadresseTypeV))
                .post(formBody)
                .build();


        Call call = client.newCall(request);

        call.enqueue(new Callback() {

            public void onResponse(Call call, Response response) throws IOException {
                String responseStr = response.body().string();
                JSONArray jsonArrayCateg = null;

                Log.d("", responseStr+" Reponse du spinner a la selection de la categorie");
                try {
                    jsonArrayCateg = new JSONArray(responseStr);

                    for (int i = 0; i < jsonArrayCateg.length(); i++) {
                        JSONObject jsonCateg = null;
                        jsonCateg = jsonArrayCateg.getJSONObject(i);
                        arrayCategV.add(jsonCateg.getString("LIBELLETYPEV"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                runOnUiThread(() -> {

                    ArrayAdapter<String> arrayAdapterType = new ArrayAdapter<String>(AjoutVoutureActivity.this, android.R.layout.simple_spinner_item, arrayCategV);

                    spinType.setAdapter(arrayAdapterType);

                });

            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }
        });
    }





    /***
     *      ____                               _   _
     *     |  _ \    ___   _ __ ___    _ __   | | (_)  ___   ___    __ _    __ _    ___
     *     | |_) |  / _ \ | '_ ` _ \  | '_ \  | | | | / __| / __|  / _` |  / _` |  / _ \
     *     |  _ <  |  __/ | | | | | | | |_) | | | | | \__ \ \__ \ | (_| | | (_| | |  __/
     *     |_| \_\  \___| |_| |_| |_| | .__/  |_| |_| |___/ |___/  \__,_|  \__, |  \___|
     *                                |_|                                  |___/
     *                    _                                __     __  _   _   _
     *      ___   _ __   (_)  _ __    _ __     ___   _ __  \ \   / / (_) | | | |   ___
     *     / __| | '_ \  | | | '_ \  | '_ \   / _ \ | '__|  \ \ / /  | | | | | |  / _ \
     *     \__ \ | |_) | | | | | | | | | | | |  __/ | |      \ V /   | | | | | | |  __/
     *     |___/ | .__/  |_| |_| |_| |_| |_|  \___| |_|       \_/    |_| |_| |_|  \___|
     *           |_|
     */
    public void listeVilles(Spinner spinville, Spinner spinstation ) throws IOException {

        OkHttpClient client = new OkHttpClient();
        ArrayList arrayListvilles = new ArrayList<String>();

        Request request = new Request.Builder()
                .url(getString(R.string.ipadresselistVilles))
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {


            public void onResponse(Call call, Response response) throws IOException {

                String responseStr = response.body().string();
                JSONArray jsonArrayVilles = null;


                try {
                    jsonArrayVilles = new JSONArray(responseStr);

                    for (int i = 0; i < jsonArrayVilles.length(); i++) {
                        JSONObject jsonVille = null;
                        jsonVille = jsonArrayVilles.getJSONObject(i);
                        arrayListvilles.add(jsonVille.getString("VILLESTATION"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                runOnUiThread(() -> { //on met le code a tourner en asyncrone , on le fait mouliner de coté, on a donc l'inteface mais les operations elles se font de tek

                    ArrayList<String> oui = new ArrayList<>();
                    oui.add("Selectionnez une ville");
                    ArrayAdapter<String> arrayAdapterVoitures = new ArrayAdapter<String>(AjoutVoutureActivity.this, android.R.layout.simple_spinner_item, oui);

                    spinville.setAdapter(arrayAdapterVoitures);

                    spinville.setOnTouchListener(new View.OnTouchListener(){

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            ArrayAdapter<String> arrayAdapterVoitures = new ArrayAdapter<String>(AjoutVoutureActivity.this, android.R.layout.simple_spinner_item, arrayListvilles);
                            spinville.setAdapter(arrayAdapterVoitures);

                            spinville.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {

                                    String item = (String) spinville.getSelectedItem();
                                    Log.d("test",item);
                                    try {
                                        remplirStation(item,spinstation);
                                    } catch (IOException e) {
                                        Log.d("test", "le remplissage du spinCateg ne marche pas =(");
                                    }
                                    spinstation.setVisibility(View.VISIBLE);
                                }
                                public void onNothingSelected(AdapterView<?> arg0) { }
                            });
                            return false;
                        }

                    });
                });
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }
        });
    }



    /***
     *      ____                               _   _
     *     |  _ \    ___   _ __ ___    _ __   | | (_)  ___   ___    __ _    __ _    ___
     *     | |_) |  / _ \ | '_ ` _ \  | '_ \  | | | | / __| / __|  / _` |  / _` |  / _ \
     *     |  _ <  |  __/ | | | | | | | |_) | | | | | \__ \ \__ \ | (_| | | (_| | |  __/
     *     |_| \_\  \___| |_| |_| |_| | .__/  |_| |_| |___/ |___/  \__,_|  \__, |  \___|
     *                                |_|                                  |___/
     *                    _                                 ____    _             _     _
     *      ___   _ __   (_)  _ __    _ __     ___   _ __  / ___|  | |_    __ _  | |_  (_)   ___    _ __
     *     / __| | '_ \  | | | '_ \  | '_ \   / _ \ | '__| \___ \  | __|  / _` | | __| | |  / _ \  | '_ \
     *     \__ \ | |_) | | | | | | | | | | | |  __/ | |     ___) | | |_  | (_| | | |_  | | | (_) | | | | |
     *     |___/ | .__/  |_| |_| |_| |_| |_|  \___| |_|    |____/   \__|  \__,_|  \__| |_|  \___/  |_| |_|
     *           |_|
     */
    public void remplirStation(String station, Spinner spinStation) throws IOException {

        OkHttpClient client = new OkHttpClient();
        ArrayList arrayStation = new ArrayList<String>();

        RequestBody formBody = new FormBody.Builder()
                .add("nomStation", station )
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.ipadresselistStationsVille))
                .post(formBody)
                .build();


        Call call = client.newCall(request);

        call.enqueue(new Callback() {

            public void onResponse(Call call, Response response) throws IOException {
                String responseStr = response.body().string();
                JSONArray jsonArrayStation = null;

                Log.d("", responseStr+" Reponse du spinnerStation quand on choisis la ville");
                try {
                    jsonArrayStation = new JSONArray(responseStr);

                    for (int i = 0; i < jsonArrayStation.length(); i++) {
                        JSONObject jsonStation = null;
                        jsonStation = jsonArrayStation.getJSONObject(i);
                        arrayStation.add("Station N° : "+jsonStation.getString("NUMSTATION")+" , "+
                                         jsonStation.getString("LIEUSTATION"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                runOnUiThread(() -> {

                    ArrayAdapter<String> arrayAdapterStations = new ArrayAdapter<String>(AjoutVoutureActivity.this, android.R.layout.simple_spinner_item, arrayStation);

                    spinStation.setAdapter(arrayAdapterStations);


                });




            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }
        });
    }



    /***
     *      _____                           _         _                  _            __                                    ____    ____    ____
     *     | ____|  _ __   __   __   ___   (_)     __| |   ___   ___    (_)  _ __    / _|   ___    ___      ___   _ __     | __ )  |  _ \  |  _ \
     *     |  _|   | '_ \  \ \ / /  / _ \  | |    / _` |  / _ \ / __|   | | | '_ \  | |_   / _ \  / __|    / _ \ | '_ \    |  _ \  | | | | | | | |
     *     | |___  | | | |  \ V /  | (_) | | |   | (_| | |  __/ \__ \   | | | | | | |  _| | (_) | \__ \   |  __/ | | | |   | |_) | | |_| | | |_| |
     *     |_____| |_| |_|   \_/    \___/  |_|    \__,_|  \___| |___/   |_| |_| |_| |_|    \___/  |___/    \___| |_| |_|   |____/  |____/  |____/
     *
     */

    public void enregistrer(){

        Spinner spinner = (findViewById(R.id.spinnerStations));
        String numStation = spinner.getSelectedItem().toString();
        String Numstation = numStation.substring(numStation.indexOf(":")+2,numStation.indexOf(",")-1);


        EditText km = findViewById(R.id.editTextTextKmVehicule);
        Spinner codetypev = findViewById(R.id.spinnerTypeV);
        String KM = km.getText().toString();

        NumberPicker nvE = findViewById(R.id.nbpNvE);


        OkHttpClient client = new OkHttpClient();


     Log.d("test",Numstation);
     Log.d("test",codetypev.getSelectedItem().toString());
     Log.d("test", KM);
     Log.d("test", String.valueOf(nvE.getValue()));
        RequestBody formBody = new FormBody.Builder()
                .add("numStation", Numstation)
                .add("codeTypeV", codetypev.getSelectedItem().toString())
                .add("KM", KM )
                .add("nvE", String.valueOf(nvE.getValue()))
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.ipadresselistAjoutVoiture))
                .post(formBody)
                .build();


        Call call = client.newCall(request);

        call.enqueue(new Callback() {

            public void onResponse(Call call, Response response) throws IOException {


            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }
        });
    }

}