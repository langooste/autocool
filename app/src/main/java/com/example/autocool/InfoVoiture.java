 package com.example.autocool;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class InfoVoiture extends AppCompatActivity {


    JSONObject jsonVoiture = null;
    String idVoiture= null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_voiture);

        try {
            final JSONObject laVoiture = new JSONObject(getIntent().getStringExtra("laVoiture"));
            idVoiture = laVoiture.getString("NUMVEHICULE");
            Log.d("test", idVoiture);

        } catch (JSONException e) {
           Log.d("test", "pas de recuperation de l'objet envoyé precedamment");
        }


        final TextView textdescrip = findViewById(R.id.txtViewTitre);


        textdescrip.setText(textdescrip.getText()+idVoiture);
        try {
            caracVoiture();
        } catch (IOException e) {
            Log.d("", "marche pas le remplissage");
        }

        findViewById(R.id.btnRetourListeV).setOnClickListener(v -> {
            finish();
        });
    }

    public void caracVoiture() throws IOException {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("IDVOITURE", idVoiture)
                .build();
        Request request = new Request.Builder()
                .url(getString(R.string.ipadressecaracVoitures))
                .post(formBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {

            public void onResponse(Call call, Response response) throws IOException {
                String responseStr = response.body().string();


                try {
                    jsonVoiture = new JSONObject(responseStr);
                } catch (JSONException e) {
                    Log.d("","pas de recup du json object depuis la reponse");
                }

                runOnUiThread(() -> {

                    try {
                        ((TextView)findViewById(R.id.txtViewNumV)).setText(((TextView)findViewById(R.id.txtViewNumV)).getText()+idVoiture);
                        ((TextView)findViewById(R.id.txtViewStation)).setText(((TextView)findViewById(R.id.txtViewStation)).getText()+jsonVoiture.getString("LIEUSTATION"));
                        ((TextView)findViewById(R.id.txtViewVille)).setText(((TextView)findViewById(R.id.txtViewVille)).getText()+jsonVoiture.getString("VILLESTATION"));
                        ((TextView)findViewById(R.id.txtViewTypeV)).setText(((TextView)findViewById(R.id.txtViewTypeV)).getText()+jsonVoiture.getString("LIBELLETYPEV"));
                        ((TextView)findViewById(R.id.txtViewNbPlaces)).setText(((TextView)findViewById(R.id.txtViewNbPlaces)).getText()+jsonVoiture.getString("NBPLACES"));
                        ((TextView)findViewById(R.id.txtViewAutomatic)).setText( ((TextView)findViewById(R.id.txtViewAutomatic)).getText()+jsonVoiture.getString("AUTOMATIQUE"));
                        ((TextView)findViewById(R.id.txtViewKmAge)).setText(((TextView)findViewById(R.id.txtViewKmAge)).getText()+jsonVoiture.getString("KILOMETRAGE")+" Km");
                        ((TextView)findViewById(R.id.txtViewNvEssence)).setText(((TextView)findViewById(R.id.txtViewNvEssence)).getText()+(jsonVoiture.getString("NIVEAUESSENCE")).substring(0,(jsonVoiture.getString("NIVEAUESSENCE")).indexOf(".")) + " %");

                    } catch (JSONException e) {;
                        Log.d("","pas de mise dans les textview des infos");
                    };

                });

            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.d("msg","pas de recup de'info en general");
            }
        });
    }
}