-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 12 mai 2021 à 19:05
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `autocool`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonne`
--

DROP TABLE IF EXISTS `abonne`;
CREATE TABLE IF NOT EXISTS `abonne` (
  `NUMABONNE` bigint(4) NOT NULL AUTO_INCREMENT,
  `CODEFORMULE` bigint(4) NOT NULL,
  `MDP` varchar(128) NOT NULL,
  `NOM` varchar(128) DEFAULT NULL,
  `PRENOM` varchar(128) DEFAULT NULL,
  `DATENAISSANCE` date DEFAULT NULL,
  `RUE` varchar(128) DEFAULT NULL,
  `VILLE` varchar(128) DEFAULT NULL,
  `CODEPOSTAL` varchar(128) DEFAULT NULL,
  `TEL` varchar(128) DEFAULT NULL,
  `TELMOBILE` varchar(128) DEFAULT NULL,
  `EMAIL` varchar(128) DEFAULT NULL,
  `NUMPERMIS` varchar(128) DEFAULT NULL,
  `LIEUPERMIS` varchar(128) DEFAULT NULL,
  `DATEPERMIS` date DEFAULT NULL,
  `PAIEMENTADHESION` tinyint(1) DEFAULT NULL,
  `PAIEMENTCAUTION` tinyint(1) DEFAULT NULL,
  `RIBFOURNIS` tinyint(1) DEFAULT NULL,
  `statut` varchar(5) NOT NULL,
  PRIMARY KEY (`NUMABONNE`),
  KEY `FK_ABONNE_FORMULE` (`CODEFORMULE`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `abonne`
--

INSERT INTO `abonne` (`NUMABONNE`, `CODEFORMULE`, `MDP`, `NOM`, `PRENOM`, `DATENAISSANCE`, `RUE`, `VILLE`, `CODEPOSTAL`, `TEL`, `TELMOBILE`, `EMAIL`, `NUMPERMIS`, `LIEUPERMIS`, `DATEPERMIS`, `PAIEMENTADHESION`, `PAIEMENTCAUTION`, `RIBFOURNIS`, `statut`) VALUES
(1, 1, '7d5c009e4eb8bbc78647caeca308e61b', 'higito', 'higuto', '2021-03-09', 'rue de bordeaux', 'bordeaux', '33000', '07.34.35.54.21', '07.34.35.54.22', 'ui', '000000000', 'bordeaux', '2021-03-16', 1, 1, 1, 'M1'),
(3, 1, 'ui', 'moi', 'moi', '2021-03-10', 'oui', 'oui', '33000', NULL, '01è_tOIU', 'sf f fd', 'df fdf ', 'df df ', '2021-03-10', NULL, NULL, NULL, 'M2');

-- --------------------------------------------------------

--
-- Structure de la table `categorie_vehicule`
--

DROP TABLE IF EXISTS `categorie_vehicule`;
CREATE TABLE IF NOT EXISTS `categorie_vehicule` (
  `CODECATEG` bigint(4) NOT NULL AUTO_INCREMENT,
  `LIBELLECATEG` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`CODECATEG`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categorie_vehicule`
--

INSERT INTO `categorie_vehicule` (`CODECATEG`, `LIBELLECATEG`) VALUES
(1, 'S'),
(2, 'M'),
(3, 'L');

-- --------------------------------------------------------

--
-- Structure de la table `facturer1`
--

DROP TABLE IF EXISTS `facturer1`;
CREATE TABLE IF NOT EXISTS `facturer1` (
  `CODEFORMULE` bigint(4) NOT NULL,
  `CODETRANCHEH` bigint(4) NOT NULL,
  `CODECATEG` bigint(4) NOT NULL,
  `TARIFH` double(5,2) DEFAULT NULL,
  PRIMARY KEY (`CODEFORMULE`,`CODETRANCHEH`,`CODECATEG`),
  KEY `FK_FACTURER1_TRANCHE_HORRAIRE` (`CODETRANCHEH`),
  KEY `FK_FACTURER1_CATEGORIE_VEHICULE` (`CODECATEG`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `facturer2`
--

DROP TABLE IF EXISTS `facturer2`;
CREATE TABLE IF NOT EXISTS `facturer2` (
  `CODETRANCHEKM` bigint(4) NOT NULL,
  `CODEFORMULE` bigint(4) NOT NULL,
  `CODECATEG` bigint(4) NOT NULL,
  `TARIFKM` int(2) DEFAULT NULL,
  PRIMARY KEY (`CODETRANCHEKM`,`CODEFORMULE`,`CODECATEG`),
  KEY `FK_FACTURER2_FORMULE` (`CODEFORMULE`),
  KEY `FK_FACTURER2_CATEGORIE_VEHICULE` (`CODECATEG`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `formule`
--

DROP TABLE IF EXISTS `formule`;
CREATE TABLE IF NOT EXISTS `formule` (
  `CODEFORMULE` bigint(4) NOT NULL AUTO_INCREMENT,
  `LIBELLEFORMULE` varchar(128) DEFAULT NULL,
  `FRAISADHESION` double(5,2) DEFAULT NULL,
  `TARIFMENSUEL` double(5,2) DEFAULT NULL,
  `PARTSOCIALE` int(2) DEFAULT NULL,
  `DEPOTGARANTIE` double(5,2) DEFAULT NULL,
  `CAUTION` double(5,2) DEFAULT NULL,
  PRIMARY KEY (`CODEFORMULE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `formule`
--

INSERT INTO `formule` (`CODEFORMULE`, `LIBELLEFORMULE`, `FRAISADHESION`, `TARIFMENSUEL`, `PARTSOCIALE`, `DEPOTGARANTIE`, `CAUTION`) VALUES
(1, 'Classique', 0.00, 8.50, 500, 0.00, 0.00),
(2, 'Classique', 0.00, 8.50, 500, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Structure de la table `station`
--

DROP TABLE IF EXISTS `station`;
CREATE TABLE IF NOT EXISTS `station` (
  `NUMSTATION` bigint(4) NOT NULL AUTO_INCREMENT,
  `LIEUSTATION` varchar(128) DEFAULT NULL,
  `VILLESTATION` varchar(128) DEFAULT NULL,
  `CPSTATION` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`NUMSTATION`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `station`
--

INSERT INTO `station` (`NUMSTATION`, `LIEUSTATION`, `VILLESTATION`, `CPSTATION`) VALUES
(5, '5 rue Albert premier', 'Bordeaux', '33000'),
(6, '15 rue jean jaures', 'Vanves', '92170'),
(7, '9 rue jean mermoz', 'Versailles', '78000'),
(8, '23 boulevard albert premier', 'Paris', '75008');

-- --------------------------------------------------------

--
-- Structure de la table `tranche_horraire`
--

DROP TABLE IF EXISTS `tranche_horraire`;
CREATE TABLE IF NOT EXISTS `tranche_horraire` (
  `CODETRANCHEH` bigint(4) NOT NULL AUTO_INCREMENT,
  `DUREE` int(2) DEFAULT NULL,
  PRIMARY KEY (`CODETRANCHEH`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tranche_km`
--

DROP TABLE IF EXISTS `tranche_km`;
CREATE TABLE IF NOT EXISTS `tranche_km` (
  `CODETRANCHEKM` bigint(4) NOT NULL AUTO_INCREMENT,
  `MINKM` int(2) DEFAULT NULL,
  `MAXKM` int(2) DEFAULT NULL,
  PRIMARY KEY (`CODETRANCHEKM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `type_vehicule`
--

DROP TABLE IF EXISTS `type_vehicule`;
CREATE TABLE IF NOT EXISTS `type_vehicule` (
  `CODETYPEV` bigint(4) NOT NULL AUTO_INCREMENT,
  `CODECATEG` bigint(4) NOT NULL,
  `LIBELLETYPEV` varchar(128) DEFAULT NULL,
  `NBPLACES` int(2) DEFAULT NULL,
  `AUTOMATIQUE` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`CODETYPEV`),
  KEY `FK_TYPE_VEHICULE_CATEGORIE_VEHICULE` (`CODECATEG`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_vehicule`
--

INSERT INTO `type_vehicule` (`CODETYPEV`, `CODECATEG`, `LIBELLETYPEV`, `NBPLACES`, `AUTOMATIQUE`) VALUES
(7, 1, 'City', 5, 2),
(8, 2, 'Break', 5, 2),
(9, 3, 'Util', 2, 1),
(10, 3, 'Poly', 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `vehicule`
--

DROP TABLE IF EXISTS `vehicule`;
CREATE TABLE IF NOT EXISTS `vehicule` (
  `NUMVEHICULE` bigint(4) NOT NULL AUTO_INCREMENT,
  `NUMSTATION` bigint(4) NOT NULL,
  `CODETYPEV` bigint(4) NOT NULL,
  `KILOMETRAGE` int(255) DEFAULT NULL,
  `NIVEAUESSENCE` double(5,2) DEFAULT NULL,
  PRIMARY KEY (`NUMVEHICULE`),
  KEY `FK_VEHICULE_STATION` (`NUMSTATION`),
  KEY `FK_VEHICULE_TYPE_VEHICULE` (`CODETYPEV`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `vehicule`
--

INSERT INTO `vehicule` (`NUMVEHICULE`, `NUMSTATION`, `CODETYPEV`, `KILOMETRAGE`, `NIVEAUESSENCE`) VALUES
(7, 6, 7, 28000, 100.00),
(8, 5, 8, 0, 29.00),
(9, 6, 7, 100000, 50.00),
(10, 5, 7, 10500, 98.00),
(11, 5, 7, 10500, 98.00),
(13, 5, 7, 10500, 98.00),
(14, 5, 7, 10587, 100.00),
(17, 8, 7, 2345, 100.00),
(18, 5, 7, 10500, 98.00),
(19, 5, 7, 10500, 98.00),
(20, 8, 7, 288665, 97.00),
(25, 6, 9, 284065, 5.00),
(26, 6, 7, 10000, 15.00),
(27, 6, 7, 10000, 15.00),
(28, 6, 7, 10000, 15.00);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `abonne`
--
ALTER TABLE `abonne`
  ADD CONSTRAINT `abonne_ibfk_1` FOREIGN KEY (`CODEFORMULE`) REFERENCES `formule` (`CODEFORMULE`);

--
-- Contraintes pour la table `facturer1`
--
ALTER TABLE `facturer1`
  ADD CONSTRAINT `facturer1_ibfk_1` FOREIGN KEY (`CODEFORMULE`) REFERENCES `formule` (`CODEFORMULE`),
  ADD CONSTRAINT `facturer1_ibfk_2` FOREIGN KEY (`CODETRANCHEH`) REFERENCES `tranche_horraire` (`CODETRANCHEH`),
  ADD CONSTRAINT `facturer1_ibfk_3` FOREIGN KEY (`CODECATEG`) REFERENCES `categorie_vehicule` (`CODECATEG`);

--
-- Contraintes pour la table `facturer2`
--
ALTER TABLE `facturer2`
  ADD CONSTRAINT `facturer2_ibfk_1` FOREIGN KEY (`CODETRANCHEKM`) REFERENCES `tranche_km` (`CODETRANCHEKM`),
  ADD CONSTRAINT `facturer2_ibfk_2` FOREIGN KEY (`CODEFORMULE`) REFERENCES `formule` (`CODEFORMULE`),
  ADD CONSTRAINT `facturer2_ibfk_3` FOREIGN KEY (`CODECATEG`) REFERENCES `categorie_vehicule` (`CODECATEG`);

--
-- Contraintes pour la table `type_vehicule`
--
ALTER TABLE `type_vehicule`
  ADD CONSTRAINT `type_vehicule_ibfk_1` FOREIGN KEY (`CODECATEG`) REFERENCES `categorie_vehicule` (`CODECATEG`);

--
-- Contraintes pour la table `vehicule`
--
ALTER TABLE `vehicule`
  ADD CONSTRAINT `vehicule_ibfk_1` FOREIGN KEY (`NUMSTATION`) REFERENCES `station` (`NUMSTATION`),
  ADD CONSTRAINT `vehicule_ibfk_2` FOREIGN KEY (`CODETYPEV`) REFERENCES `type_vehicule` (`CODETYPEV`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
